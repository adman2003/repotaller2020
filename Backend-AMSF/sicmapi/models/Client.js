const mongoose = require('mongoose');

const clientSchema = mongoose.Schema({
    idcliente:Number,
    nombre:String,
    apellido:String,
    cuenta: String,
    listado:[{
        fecha:String,
        importe:Number,
        tipo:String
    }]
});

module.exports = mongoose.model('Cliente', clientSchema);