const Cliente = require('../../models/Client');

function updateClient(req, res){
    const id = req.params.id;
    const nombre = req.body.nombre;
    const apellido = req.body.apellido;
    const cuenta = req.body.cuenta;

    Cliente.find()
    .where('_id').equals(id)
    .update({
        nombre:nombre,
        apellido:apellido,
        cuenta:cuenta
    })
  .then(function(result){
      res.json({
        message: 'Cliente Actualizado'
      })
    })
    .catch(function(err){
      res.json({
        error: err
      })
    })

}

module.exports = updateClient;