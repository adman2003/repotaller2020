const express = require('express');
const router = express.Router();
const port = 8080;
router.use(express.json());

const miAppSicm = require("./miAppSicm");
router.use('/miAppSicm', miAppSicm);

router.all('/', function (request, response){
    response.json({
        "General":`http://localhost${port}/sicmapi`
    })
})

module.exports = router;